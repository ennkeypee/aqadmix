require 'sinatra'
require 'sinatra/reloader'
require 'haml'
require 'data_mapper'

DataMapper::setup(:default,"sqlite3://#{Dir.pwd}/example.db")

class Topic
  include DataMapper::Resource
  property :id, Serial
  property :title, String
  property :ennkeypee_content, Text, :default => "No Content posted by you yet"
  property :ennkeypee_followers, Integer
  property :ennkeypee_trust, Integer, :default => 0
  property :ennkeypee_created_at, Time, :default => Time.now
 
end

class Chapter
  include DataMapper::Resource
  property :id, Serial
  property :title, String
end

DataMapper.finalize.auto_upgrade!

get '/topic' do
  @topics = Topic.all
  @topictodisplay = Topic.first(id: 1)
  haml :topic
end

get '/chapter' do
  @chapters = Chapter.all
  haml :chapter
end

post '/topic' do
  l = Topic.new
  l.title = params[:ennkeypee_content]
  l.trust = params[:trust]
  l.created_at = Time.now
  l.save
  redirect back
end

put '/:id/vote/:type' do
  l = Topic.get params[:id]
  l.save
  redirect back
end






configure do
  enable :sessions
  set :email_id, 'ennkeypee@gmail.com'
  set :password, 'ennkeypee'
  set :views, 'views'
  set :public_folder, 'public'
end

get '/start' do
 haml :start
end

post '/start' do
  if params[:email_id] == settings.email_id
     session[:admin] = true
     redirect to('/password')
  else
     haml :join
  end
end

get '/password' do
 haml :password
end

post '/password' do
  if params[:password] == settings.password
     session[:admin] = true
     redirect to('/course')
  else
     haml :join
  end
end

get '/join' do
 haml :join
end

get '/' do
  haml :aqadmix, :layout => :aqadmixlayout
end

get '/course' do
  redirect to('/start') unless session[:admin]
  @title = "Progress"
  haml :course
end

get '/aboutus' do
  @title = "About us"
  haml :aboutus
end

get '/news' do
  @title = "News"
  haml :news
end

get '/search' do
 haml :search
end


not_found do
  @title = "Error"
  haml :not_found
end

get '/logout' do
session.clear
redirect to('/')
end

